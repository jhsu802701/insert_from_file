require 'spec_helper'

RSpec.describe InsertFromFile do
  it 'has a version number' do
    expect(InsertFromFile::VERSION).not_to be nil
  end

  it 'does something useful' do
    expect(true).to eq(true)
  end

  describe 'has working functionality' do
    require 'insert_from_file'
    system('rm -rf tmp')
    system('mkdir tmp')

    it 'add_before function works' do
      file_w = open('tmp/file1.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("ten eleven twelve\n")
      file_w.write("thirteen fourteen fifteen\n")
      file_w.close
      file_w = open('tmp/file2.txt', 'w')
      file_w.write("four five six\n")
      file_w.write("seven eight nine\n")
      file_w.close
      InsertFromFile.add_before('tmp/file2.txt', 'tmp/file1.txt', 'ten')
      array_lines = IO.readlines('tmp/file1.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq("thirteen fourteen fifteen\n")
      system('rm tmp/file1.txt')
      system('rm tmp/file2.txt')
    end

    it 'add_after function works' do
      file_w = open('tmp/file3.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.write("thirteen fourteen fifteen\n")
      file_w.close
      file_w = open('tmp/file4.txt', 'w')
      file_w.write("seven eight nine\n")
      file_w.write("ten eleven twelve\n")
      file_w.close
      InsertFromFile.add_after('tmp/file4.txt', 'tmp/file3.txt', 'six')
      array_lines = IO.readlines('tmp/file3.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq("thirteen fourteen fifteen\n")
      system('rm tmp/file3.txt')
      system('rm tmp/file4.txt')
    end

    it 'replace function works' do
      file_w = open('tmp/file5.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.write("sixteen seventeen eighteen\n")
      file_w.write("thirteen fourteen fifteen\n")
      file_w.close
      file_w = open('tmp/file6.txt', 'w')
      file_w.write("seven eight nine\n")
      file_w.write("ten eleven twelve\n")
      file_w.close
      InsertFromFile.replace('tmp/file6.txt', 'tmp/file5.txt', 'sixteen')
      array_lines = IO.readlines('tmp/file5.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq("thirteen fourteen fifteen\n")
      system('rm tmp/file5.txt')
      system('rm tmp/file6.txt')
    end

    it 'add_before function works at start of file' do
      file_w = open('tmp/file7.txt', 'w')
      file_w.write("seven eight nine\n")
      file_w.write("ten eleven twelve\n")
      file_w.write("thirteen fourteen fifteen\n")
      file_w.close
      file_w = open('tmp/file8.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.close
      InsertFromFile.add_before('tmp/file8.txt', 'tmp/file7.txt', 'eight')
      array_lines = IO.readlines('tmp/file7.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq("thirteen fourteen fifteen\n")
      system('rm tmp/file7.txt')
      system('rm tmp/file8.txt')
    end

    it 'add_after function works at end of file' do
      file_w = open('tmp/file9.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.write("seven eight nine\n")
      file_w.close
      file_w = open('tmp/file10.txt', 'w')
      file_w.write("ten eleven twelve\n")
      file_w.write("thirteen fourteen fifteen\n")
      file_w.close
      InsertFromFile.add_after('tmp/file10.txt', 'tmp/file9.txt', 'nine')
      array_lines = IO.readlines('tmp/file9.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq("thirteen fourteen fifteen\n")
      system('rm tmp/file9.txt')
      system('rm tmp/file10.txt')
    end

    it 'replace_between function works' do
      file_w = open('tmp/file11.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.write("nineteen twenty twenty-one\n")
      file_w.write("thirteen fourteen fifteen\n")
      file_w.write("sixteen seventeen eighteen\n")
      file_w.close
      file_w = open('tmp/file12.txt', 'w')
      file_w.write("seven eight nine\n")
      file_w.write("ten eleven twelve\n")
      file_w.close
      f1 = 'tmp/file12.txt' # source file
      f2 = 'tmp/file11.txt' # destination file
      str1 = 'five' # string in earlier line
      str2 = 'fourteen' # string in later line
      InsertFromFile.replace_between(f1, f2, str1, str2)
      array_lines = IO.readlines('tmp/file11.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq("thirteen fourteen fifteen\n")
    end

    it 'add_beginning function works' do
      file_w = open('tmp/file13.txt', 'w')
      file_w.write("seven eight nine\n")
      file_w.write("ten eleven twelve\n")
      file_w.write("thirteen fourteen fifteen\n")
      file_w.close
      file_w = open('tmp/file14.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.close
      InsertFromFile.add_beginning('tmp/file14.txt', 'tmp/file13.txt')
      array_lines = IO.readlines('tmp/file13.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq("thirteen fourteen fifteen\n")
      system('rm tmp/file13.txt')
      system('rm tmp/file14.txt')
    end

    it 'add_end function works' do
      file_w = open('tmp/file15.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.write("seven eight nine\n")
      file_w.close
      file_w = open('tmp/file16.txt', 'w')
      file_w.write("ten eleven twelve\n")
      file_w.write("thirteen fourteen fifteen\n")
      file_w.close
      InsertFromFile.add_end('tmp/file16.txt', 'tmp/file15.txt')
      array_lines = IO.readlines('tmp/file15.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq("thirteen fourteen fifteen\n")
      system('rm tmp/file15.txt')
      system('rm tmp/file16.txt')
    end
  end
end
