lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'insert_from_file/version'

Gem::Specification.new do |spec|
  spec.name = 'insert_from_file'
  spec.version = InsertFromFile::VERSION
  spec.authors = ['Jason Hsu']
  spec.email = ['rubygems@jasonhsu.com']

  spec.summary = 'Insert the contents of one file into another file.'
  spec.description = 'Insert the contents of the source file into the destination file.  Look for a line with certain content in the destination file, and add the contents of the source file before or after this point.  You can also look for two lines with certain content in the destination file and replace everything in between with the contents of the source file.'
  spec.homepage = 'https://bitbucket.org/jhsu802701/insert_from_file'
  spec.license = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '1.16.3'
  spec.add_development_dependency 'rake', '12.3.1'
  spec.add_development_dependency 'rspec', '3.8.0'

  spec.add_development_dependency 'bundler-audit', '0.6.0'
  spec.add_development_dependency 'gemsurance', '0.9.0'
  spec.add_development_dependency 'rubocop', '0.58.2'
  spec.add_development_dependency 'ruby-graphviz', '1.2.3'
  spec.add_development_dependency 'simplecov', '0.16.1'

  spec.add_development_dependency 'codecov', '0.1.10'

  spec.add_runtime_dependency 'line_containing'
  spec.add_runtime_dependency 'string_in_file'
end
