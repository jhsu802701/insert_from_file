# InsertFromFile

[![Gem Version](https://badge.fury.io/rb/insert_from_file.svg)](https://badge.fury.io/rb/insert_from_file)
[![Build Status](https://semaphoreci.com/api/v1/jhsu802701/insert_from_file/branches/master/badge.svg)](https://semaphoreci.com/jhsu802701/insert_from_file)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/ad95b790c84c46bca9a19456a838377a)](https://www.codacy.com/app/jhsu802701/insert_from_file?utm_source=jhsu802701@bitbucket.org&amp;utm_medium=referral&amp;utm_content=jhsu802701/insert_from_file&amp;utm_campaign=Badge_Grade)
[![codecov](https://codecov.io/bb/jhsu802701/insert_from_file/branch/master/graph/badge.svg)](https://codecov.io/bb/jhsu802701/insert_from_file)

## What This Gem Does

The InsertFromFile gem is for inserting the contents of one file into another file.

## Usage

### add_before
* tmp/file1.txt:
```
one two three
ten eleven twelve
thirteen fourteen fifteen
```
* tmp/file2.txt:
```
four five six
seven eight nine
```
* Adding the contents of tmp/file2.txt before the line in tmp/file1.txt containing 'ten':
```
InsertFromFile.add_before('tmp/file2.txt', 'tmp/file1.txt', 'ten')
```

### add_after
* tmp/file3.txt:
```
one two three
four five six
thirteen fourteen fifteen
```
* tmp/file4.txt:
```
seven eight nine
ten eleven twelve
```
* Adding the contents of tmp/file4.txt after the line in tmp/file3.txt containing 'six':
```
InsertFromFile.add_after('tmp/file4.txt', 'tmp/file3.txt', 'six')
```

### replace
* tmp/file5.txt:
```
one two three
four five six
sixteen seventeen eighteen
thirteen fourteen fifteen
```
* tmp/file6.txt:
```
seven eight nine
ten eleven twelve
```
* Adding the contents of tmp/file6.txt into tmp/file5.txt in place of the line containing 'sixteen':
```
InsertFromFile.replace('tmp/file6.txt', 'tmp/file5.txt', 'sixteen')
```

### replace_between
* tmp/file11.txt:
```
one two three
four five six
nineteen twenty twenty-one
thirteen fourteen fifteen
sixteen seventeen eighteen
```
* tmp/file12.txt:
```
seven eight nine
ten eleven twelve
```
* Adding the contents of tmp/file12.txt into tmp/file11.txt and replacing everything after the line containing "five" and before the line containing "fourteen":
```
InsertFromFile.replace_between('tmp/file12.txt', 'tmp/file11.txt', 'five', 'fourteen')
```

### add_beginning
* tmp/file13.txt:
```
seven eight nine
ten eleven twelve
thirteen fourteen fifteen
```
* tmp/file14.txt:
```
one two three
four five six
```
* Adding the contents of tmp/file14.txt to the beginning of tmp/file13.txt:
```
InsertFromFile.add_beginning('tmp/file14.txt', 'tmp/file13.txt')
```

### add_end
* tmp/file15.txt:
```
one two three
four five six
seven eight nine
```
* tmp/file16.txt:
```
ten eleven twelve
thirteen fourteen fifteen
```
* Adding the contents of temp/file16.txt to the beginning of tmp/file15.txt:
```
InsertFromFile.add_end('tmp/file16.txt', 'tmp/file15.txt')
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'insert_from_file'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install insert_from_file

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/jhsu802701/insert_from_file. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Bash Scripts
### Testing this gem
After you download this source code, enter `sh gem_test.sh` to set up and test this gem.

### Testing this gem's source code
Enter `sh code_test.sh` to test the quality of this gem's source code.
### Running this gem in irb
Enter `sh gem_console.sh`.

### Installing this gem
Enter `sh gem_install.sh`.
### Testing the gem, source code, and installation process
Enter `sh all.sh` to run the gem_test.sh, code_test.sh, and gem_install.sh scripts.
