# rubocop:disable Metrics/MethodLength
require 'insert_from_file/version'
require 'line_containing'
require 'string_in_file'

#
module InsertFromFile
  def self.add_before(file_source, file_dest, str_dest)
    path_old = file_dest
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    File.readlines(path_old).each do |line|
      if line.include? str_dest
        File.readlines(file_source).each do |line_s|
          file_w.write(line_s)
        end
      end
      file_w.write(line)
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.add_after(file_source, file_dest, str_dest)
    path_old = file_dest
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    File.readlines(path_old).each do |line|
      file_w.write(line)
      # rubocop:disable Style/Next
      if line.include? str_dest
        File.readlines(file_source).each do |line_s|
          file_w.write(line_s)
        end
      end
      # rubocop:enable Style/Next
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.replace(file_source, file_dest, str_dest)
    path_old = file_dest
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    File.readlines(path_old).each do |line|
      if line.include? str_dest
        File.readlines(file_source).each do |line_s|
          file_w.write(line_s)
        end
      else
        file_w.write(line)
      end
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.replace_between(file_source, file_dest, str1, str2)
    LineContaining.delete_between(str1, str2, file_dest)
    path_old = file_dest
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    is_before = true
    is_after = false
    File.readlines(path_old).each do |line|
      if is_before == true # Beginning
        file_w.write(line)
        if line.include? str1
          File.readlines(file_source).each do |line_s|
            file_w.write(line_s)
          end
        end
      elsif is_after == true # End
        file_w.write(line)
      elsif line.include? str2 # Middle
        file_w.write(line)
        is_after = true
      end
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.add_beginning(file_source, file_dest)
    path_old = file_dest
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    File.readlines(file_source).each do |line|
      file_w.write(line)
    end
    File.readlines(path_old).each do |line|
      file_w.write(line)
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.add_end(file_source, file_dest)
    path_old = file_dest
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    File.readlines(path_old).each do |line|
      file_w.write(line)
    end
    File.readlines(file_source).each do |line|
      file_w.write(line)
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end
end
# Your new gem is a module by default.  You may wish to use a class instead.
# rubocop:enable Metrics/MethodLength
